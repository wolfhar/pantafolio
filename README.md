# My Devops Practices I: Cloud website with CI/CD pipeline



## Summary

This repo is a simple static js/html website automatically hosted on an oracle free-tier instance by an CI/CD pipeline.

## Useful Files

- [ ] [.gitlabci-yml](https://gitlab.com/wolfhar/pantafolio/-/blob/main/.gitlab-ci.yml?ref_type=heads) Its a pipeline script, which contains every step to deploy and update the website each time a commit is pushed.
- [ ] [index.html](https://gitlab.com/wolfhar/pantafolio/-/blob/main/index.html?ref_type=heads) index.html is the actual website, containing small snippets of JavaScript. Its a simple file used only to deploy something.

## Tools

- [ ] [Oracle](https://www.oracle.com/br/cloud/) To host the virtual instance on which the deployment will be performed.
- [ ] [Apache](https://httpd.apache.org) To use as a server for the html file.
- [ ] [Gitlab](https://gitlab.com/) For hosting the repository and running the cicd pipeline.

## Steps

- [X] Create an virtual instance for free on Oracle/AWS/Azure/GCP.
- [X] Configure network settings such as firewall and ports.
- [X] Download Apache and all its dependecies.
- [X] Check if an HTML file in the /var/www/html directory will be displayed at your IP.
- [X] Configure the .gitlabci-yml file to connect via ssh to the virtual instance, clone the repository and move the index.html file to /var/www/html, replacing the previous version.

## Next Steps

- [ ] Add all pré-configuration on a Ansible playbook.
- [ ] Create and deploy on an image for tests.
- [ ] Integrate with an SQL database.
- [ ] Integrate Website + SQL into Hashicorp's Terraform.
- [ ] Add NAT.
